$(".consigne-textarea").on('focus', function () {
    $(this).next().css('display', 'block');
})

$(".consigne-textarea").on('blur', function () {
    $(this).next().css('display', 'none');
})

// -----------

$(".matiere-annonce").on('click', function () {
    $(".blockSpecialite").css('display', 'block');
})

// $(".matiere-annonce").on('blur', function () {
//     $(".blockSpecialite").css('display', 'none');
// })

//------------

// custom checkbox
// $(function () {
//     var checkboxs = $('input[type=checkbox]');

//     checkboxs.each(function () {
//         $(this).wrap('<div class="customCheckbox"></div>');
//         $(this).before('<span>&#10004;</span>');
//     });

//     checkboxs.change(function () {
//         if ($(this).is(':checked')) {
//             $(this).parent().addClass('customCheckboxChecked');
//         } else {
//             $(this).parent().removeClass('customCheckboxChecked');
//         }
//     });
// })


// -----------

$('.blockSpecialite').css('display','none !important');
$('a.linkSpecialite').on('click',function(){
    let id = $(this).attr('href').split('');
    id.splice(0,1);
    id = id.join('');
    $('.blockSpecialite').each(function(){
        if($(this).attr('id') === id){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
            }else{
                $(this).addClass('active');
            }
        }else{
            $(this).removeClass('active');
        }
    })
})

// -----------

$(".checked").on('click', function () {
    if ($(this).children('input').is(':checked')) {
        $(this).children('input').removeAttr('checked');
        $(this).css('background-color', 'rgba(134, 134, 134, .1)');
        $(this).css('color', 'black');
    } else {
        this.children[0].setAttribute('checked', '');
        $(this).css('background-color', 'rgb(72 193 126)');
        $(this).css('color', 'white');
    }
})

// -----------

$(".tarifCheck1").on('change', function () {
    if ($('.tarifCheck1').is(':checked')) {
        $('.tarifDisplay1').css('display', 'block');
    } else {
        $('.tarifDisplay1').css('display', 'none');
    }
})

$(".tarifCheck2").on('change', function () {
    if ($('.tarifCheck2').is(':checked')) {
        $('.tarifDisplay2').css('display', 'block');
    } else {
        $('.tarifDisplay2').css('display', 'none');
    }
})

//------------

// $(".checked-consigne").on('click', function () {
//     if ($(this).children('input').is(':checked')) {
//         $(this).children('input').removeAttr('checked');
//         $(this).css('background-color', 'rgba(134, 134, 134, .1)');
//         $(this).css('color', 'black');
//         // $(".titre-consigne").css('display', 'none')
//     } else {
//         this.children[0].setAttribute('checked', '');
//         $(this).css('background-color', 'rgb(72 193 126)');
//         $(this).css('color', 'white');
//         // $(".titre-consigne").css('display', 'block')
//     }
// })

//------------

$(".check-langue").on('click', function () {
    $(this).toggleClass("bg-langue")
})

//-----------
// DropZone
$(function () {
    $('#dropzone').on('dragover', function () {
        $(this).addClass('hover');
    });

    $('#dropzone').on('dragleave', function () {
        $(this).removeClass('hover');
    });

    $('#dropzone input').on('change', function (e) {
        var file = this.files[0];

        $('#dropzone').removeClass('hover');

        if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
            return alert('File type not allowed.');
        }

        $('#dropzone').addClass('dropped');
        $('#dropzone img').remove();

        if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
            var reader = new FileReader(file);
            reader.readAsDataURL(file);
            reader.onload = function (e) {
            var data = e.target.result,
            $img = $('<img />').attr('src', data).fadeIn();
            $('#dropzone div').html($img);
        };
        } else {
            var ext = file.name.split('.').pop();
            $('#dropzone div').html(ext);
        }
    });
});

// ----------